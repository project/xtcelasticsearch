<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_d8path",
 *   label = @Translation("Path for XTC ElasticSearch"),
 *   description = @Translation("Path for XTC ElasticSearch.")
 * )
 */
class ElasticSearchD8path extends ElasticSearchBase {


  public function formatTo() {
    $text = ElasticSearchText::textType($this->options['field']);
    return [
      'properties' => [
        'langcode' => [
          'type' => 'keyword',
        ],
        'alias' => $text,
        'pid' => [
          'type' => 'integer',
        ],
      ],
    ];
  }

}
