<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_reference",
 *   label = @Translation("Reference for XTC ElasticSearch"),
 *   description = @Translation("Reference for XTC ElasticSearch.")
 * )
 */
class ElasticSearchReference extends ElasticSearchKeyword {

}
