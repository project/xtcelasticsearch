<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_d8er",
 *   label = @Translation("Entity Reference for XTC ElasticSearch"),
 *   description = @Translation("Entity Reference for XTC ElasticSearch.")
 * )
 */
class ElasticSearchD8er extends ElasticSearchReference {

  public function formatTo() {
    $text = ElasticSearchText::textType($this->options['field']);

    return [
      'properties' => [
        'target_id' => [
          'type' => 'keyword',
        ],
        'title' => $text,
        'name' => $text,
      ],
    ];
  }

}
