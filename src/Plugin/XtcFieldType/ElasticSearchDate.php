<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_date",
 *   label = @Translation("Date for XTC ElasticSearch"),
 *   description = @Translation("Date for XTC ElasticSearch.")
 * )
 */
class ElasticSearchDate extends ElasticSearchBase {

  public function formatTo(){
    return [
      'type' => 'date',
    ];
  }

}
