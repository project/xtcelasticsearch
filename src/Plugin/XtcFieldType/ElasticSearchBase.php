<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


use Drupal\xtcsearch\PluginManager\XtcFieldType\XtcFieldTypePluginBase;

/**
 * Plugin implementation of the xtc_fieldtype for FileType.
 *
 */
abstract class ElasticSearchBase extends XtcFieldTypePluginBase
{

}
