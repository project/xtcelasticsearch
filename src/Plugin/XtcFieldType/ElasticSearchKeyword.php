<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_keyword",
 *   label = @Translation("Keyword for XTC ElasticSearch"),
 *   description = @Translation("Keyword for XTC ElasticSearch.")
 * )
 */
class ElasticSearchKeyword extends ElasticSearchBase {

  public function formatTo(){
    return [
      'type' => 'keyword',
    ];
  }

}
