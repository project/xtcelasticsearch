<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


use Drupal\field\Entity\FieldConfig;
use Drupal\xtcelasticsearch\Plugin\XtcHandler\ElasticSearchMapping;

/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_text",
 *   label = @Translation("Text for XTC ElasticSearch"),
 *   description = @Translation("Text for XTC ElasticSearch.")
 * )
 */
class ElasticSearchText extends ElasticSearchBase {

  public function formatTo(){
    return self::textType($this->options['field']);
  }

  public static function textType($field) {
    if ($field instanceof FieldConfig) {
      if(!empty($field->get('langcode'))) {
        $analyzer = ElasticSearchMapping::getLanguage($field->get('langcode'));
        return [
          'type' => 'text',
          'analyzer' => $analyzer,
        ];
      }
    }
    return [
      'type' => 'text',
    ];
  }

}
