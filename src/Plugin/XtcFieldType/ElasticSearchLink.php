<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_link",
 *   label = @Translation("Link for XTC ElasticSearch"),
 *   description = @Translation("Link for XTC ElasticSearch.")
 * )
 */
class ElasticSearchLink extends ElasticSearchBase {

  /**
   * @return array
   */
  public function formatTo() {
    $text = ElasticSearchText::textType($this->options['field']);
    return [
      'properties' => [
        'title' => $text,
        'uri' => $text,
      ],
    ];
  }

}
