<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_boolean",
 *   label = @Translation("Boolean for XTC ElasticSearch"),
 *   description = @Translation("Boolean for XTC ElasticSearch.")
 * )
 */
class ElasticSearchBoolean extends ElasticSearchBase {

  public function formatTo(){
    return [
      'type' => 'boolean',
    ];
  }

}
