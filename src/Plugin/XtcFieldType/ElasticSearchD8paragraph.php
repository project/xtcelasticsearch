<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigBase;
use Drupal\xtcsearch\XtendedContent\API\XtcSearchLoaderFieldType;

/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_d8paragraph",
 *   label = @Translation("Paragraph for XTC ElasticSearch"),
 *   description = @Translation("Paragraph for XTC ElasticSearch.")
 * )
 */
class ElasticSearchD8paragraph extends ElasticSearchReference {

  /**
   * @var FieldConfigBase
   */
  protected $settings;

  /**
   * @return array|string[]
   */
  public function formatTo() {
    $this->settings = $this->options['field'];
    return [
      'properties' => $this->buildFieldConfig()
    ];
  }

  protected function buildFieldConfig() {
    $fields = [];
    $mapping = $this->options['mapping'];
    $fieldSettings = $this->settings->getSettings();
    $entityType = substr($fieldSettings['handler'], 8);
    $bundles = $fieldSettings['handler_settings']['target_bundles'];

    foreach ($bundles as $bundle => $description) {
      $bundleFields = \Drupal::service('entity_field.manager')
        ->getFieldDefinitions($entityType, $bundle);
      foreach ($bundleFields as $fieldName => $field) {
        if ($field instanceof BaseFieldDefinition || $field instanceof FieldConfigBase) {
          $type = $field->getType();
          $options = [
            'name' => $fieldName,
            'field' => $field,
            'mapping' => $mapping,
          ];
          $toField = XtcSearchLoaderFieldType::get($mapping[$type]['to'], $options);
          $fields[$fieldName] = $toField->formatTo();
        }
      }
    }
    return $fields;
  }

}
