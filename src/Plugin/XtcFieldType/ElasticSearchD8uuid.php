<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcelasticsearch\Plugin\XtcFieldType;


/**
 * Plugin implementation of the xtc_fieldtype.
 *
 * @XtcFieldType(
 *   id = "elasticsearch_d8uuid",
 *   label = @Translation("UUID for XTC ElasticSearch"),
 *   description = @Translation("UUID for XTC ElasticSearch.")
 * )
 */
class ElasticSearchD8uuid extends ElasticSearchKeyword {

}
