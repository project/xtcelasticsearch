<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;
use Drupal\xtcsearch\XtendedContent\Interfaces\UnindexInterface;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elasticsearch_unindex",
 *   label = @Translation("PHP Elasticsearch Unindex for XTC"),
 *   description = @Translation("PHP Elasticsearch Unindex for XTC
 *   description.")
 * )
 */
class ElasticSearchUnindex extends ElasticSearchIndex implements UnindexInterface {

  protected function adaptContent() {
  }

  protected function runProcess() {
    $this->params = array_merge($this->params, $this->options);
    $this->cleanParams();
    try {
      $this->content = $this->client->delete($this->params);
    } catch (\Exception $exception) {
      $this->content = $exception->getMessage();
    }
  }


  /**
   * @return XtcHandlerPluginBase
   */
  public function deleteContent() : XtcHandlerPluginBase {
    return $this->process();
  }

}
