<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


use Drupal\Core\Site\Settings;
use Drupal\xtc\XtendedContent\API\XtcLoaderServer;
use Drupal\xtcsearch\Plugin\XtcHandler\SearchBase;
use Elasticsearch\ClientBuilder;

/**
 * Plugin implementation of the xtc_handler for ElasticSearch.
 *
 */
abstract class ElasticSearchBase extends SearchBase {

  /**
   * @var \Elasticsearch\Client
   */
  protected $client;

  protected function setParams() {
    if (!empty($this->options['index'])) {
      $this->params = [
        'index' => $this->options['index'],
        'type' => '_doc',
      ];
    }
  }

  protected function buildClient() {
    $settings = Settings::get('xtc_servers');

    $server = XtcLoaderServer::load($this->profile['server']);
    $env = $settings[$this->profile['server']]['env'] ?? $server['env'];

    $protocol = ($server['connection'][$env]['tls']) ? 'https' : 'http';
    $host = $server['connection'][$env]['host'] . ':' . $server['connection'][$env]['port'];
    $hosts[] = $protocol . '://' . $host;

    $params = [
      'timeout' => $this->getTimeout(),
    ];

    $clientBuilder = ClientBuilder::create()
      ->setHosts($hosts)
      ->setConnectionParams($params);

    if (
      !empty($server['connection'][$env]['username']) &&
      !empty($server['connection'][$env]['password'])
    ) {
      $clientBuilder->setBasicAuthentication($server['connection'][$env]['username'], $server['connection'][$env]['password']);
    }
    $this->client = $clientBuilder->build();
  }

}
