<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elasticsearch_get",
 *   label = @Translation("PHP Elasticsearch Get for XTC"),
 *   description = @Translation("PHP Elasticsearch Get for XTC description.")
 * )
 */
class ElasticSearchGet extends ElasticSearchBase {

  const AUTHORIZED = [
    'id',
    'index',
    'type',
    'ignore_missing',
    'stored_fields',
    'parent',
    'preference',
    'realtime',
    'refresh',
    'routing',
    '_source',
    '_source_exclude',
    '_source_include',
    '_source_excludes',
    '_source_includes',
  ];

  protected function runProcess() {
    parent::runProcess();
    try {
      $this->content = $this->client->get($this->params);
    } catch (\Exception $exception) {
      $this->content = $exception->getMessage();
    }
  }

  protected function cleanParams() {
    foreach ($this->params as $name => $param){
      if (in_array($name, self::AUTHORIZED)){
        $params[$name] = $param;
      }
    }
    $this->params = $params;
  }

}
