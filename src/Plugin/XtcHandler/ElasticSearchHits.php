<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elasticsearch_hits",
 *   label = @Translation("PHP Elasticsearch Search for XTC"),
 *   description = @Translation("PHP Elasticsearch Search for XTC description.")
 * )
 */
class ElasticSearchHits extends ElasticSearchSearch {

  protected function adaptContent() {
    $results = $this->content;
    $this->content = [];
    foreach ($results['hits']['hits'] as $key => $result){
      $this->content[$key] = $result['_source'];
    }
  }

}
