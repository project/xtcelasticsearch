<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elasticsearch_info",
 *   label = @Translation("PHP Elasticsearch Server status for XTC"),
 *   description = @Translation("PHP Elasticsearch Server status for XTC
 *   description.")
 * )
 */
class ElasticSearchInfo extends ElasticSearchBase {

  protected function runProcess() {
    try {
      $this->content = $this->client->info();
    } catch (\Exception $exception) {
      $this->content = $exception->getMessage();
    }
  }

  protected function adaptContent() {
  }

}
