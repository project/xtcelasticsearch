<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


use Drupal\xtcsearch\XtendedContent\Interfaces\IndexInterface;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elasticsearch_index",
 *   label = @Translation("PHP Elasticsearch Index for XTC"),
 *   description = @Translation("PHP Elasticsearch Index for XTC description.")
 * )
 */
abstract class ElasticSearchIndexBase extends ElasticSearchBase implements IndexInterface {

  protected $response;

  /**
   * @return $this|\Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  public function process() {
    $this->initProcess();
    $this->adaptContent();
    $this->runProcess();
    return $this;
  }

  public function getResponse() {
    return $this->response ?? [];
  }

}
