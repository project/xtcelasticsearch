<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elasticsearch_search",
 *   label = @Translation("PHP Elasticsearch Search for XTC"),
 *   description = @Translation("PHP Elasticsearch Search for XTC description.")
 * )
 */
class ElasticSearchSearch extends ElasticSearchBase {

  const AUTHORIZED = [
    'index',
    'type',
    'analyzer',
    'analyze_wildcard',
    'default_operator',
    'df',
    'explain',
    'fields',
    'stored_fields',
    'from',
    'ignore_indices',
    'indices_boost',
    'lenient',
    'lowercase_expanded_terms',
    'preference',
    'q',
    'query_cache',
    'request_cache',
    'routing',
    'scroll',
    'search_type',
    'size',
    'sort',
    'source',
    '_source',
    '_source_exclude',
    '_source_include',
    '_source_excludes',
    '_source_includes',
    'stats',
    'suggest_field',
    'suggest_mode',
    'suggest_size',
    'suggest_text',
    'timeout',
    'terminate_after',
    'version',
    'body',
  ];

  protected function runProcess() {
    parent::runProcess();

    try {
      $this->content = $this->client->search($this->params);
    } catch (\Exception $exception) {
      $this->content = $exception->getMessage();
    }
  }

  protected function cleanParams() {
    foreach ($this->params as $name => $param){
      if (in_array($name, self::AUTHORIZED)){
        $params[$name] = $param;
      }
    }
    $this->params = $params;
  }

}
