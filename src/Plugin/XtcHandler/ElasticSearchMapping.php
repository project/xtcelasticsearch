<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigBase;
use Drupal\xtcsearch\XtendedContent\API\XtcSearchLoaderFieldType;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elasticsearch_mapping",
 *   label = @Translation("PHP Elasticsearch Mapping for XTC"),
 *   description = @Translation("PHP Elasticsearch Mapping for XTC
 *   description.")
 * )
 */
class ElasticSearchMapping extends ElasticSearchBase {

  const LANG = [
//    'en' => 'xtc_english',
    'fr' => 'xtc_french',
  ];

  const AUTHORIZED = [
    'index',
    'include_type_name',
    'wait_for_active_shards',
    'timeout',
    'master_timeout',
    'body',
  ];

  protected $response;

  /**
   * @var array
   */
  protected $mappings = [];

  /**
   * @return $this|\Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  public function process() {
    $this->initProcess();
    $this->adaptContent();
    $this->runProcess();
    return $this;
  }

  protected function adaptContent() {
    foreach ($this->options['fields'] as $mapping => $settings) {
      $params['index'] = $settings['index'];
      if ($this->client->indices()->exists($params)) {
        $this->content[$mapping] = $params['index'] . ": This index already exists thus can't be created.";
        $this->getIndexMapping($settings['index'], $mapping);
      }
      else {
        try {
          $this->buildSettings($mapping);
          $this->buildMapping($mapping);
        } catch (\Exception $exception) {
          $this->content[$mapping] = Json::decode($exception->getMessage());
        }
      }
    }
  }

  protected function getIndexMapping($index, $mapping) {
    $this->params = [
      'index' => $index,
    ];
    try {
      $this->content['mapping'][$mapping] = $this->client->indices()
        ->get($this->params);
    } catch (\Exception $exception) {
      $this->content[$mapping] = Json::decode($exception->getMessage());
    }
  }

  protected function buildSettings($name) {
    $analyser = ElasticSearchMapping::getLanguage('fr');
    $settings = [
      "analysis" => [
        "analyzer" => [
          $analyser => [
            "type" => "custom",
//            "tokenizer" => "standard",
            "tokenizer" => "icu_tokenizer",
            "filter" => [
              "lowercase",
              "stop_francais",
              "fr_stemmer",
//              "asciifolding",
              "icu_folding",
              "elision",
            ],
          ],
          "autocomplete" => [
            "type" => "custom",
//            "tokenizer" => "standard",
            "tokenizer" => "icu_tokenizer",
            "filter" => [
              "elision",
              "lowercase",
              "asciifolding",
              "icu_folding",
//              "autocomplete",
            ],
          ],
        ],
        "filter" => [
          "stop_francais" => [
            "type" => "stop",
            "stopwords" => [
              "_french_",
            ],
          ],
          "fr_stemmer" => [
            "type" => "stemmer",
            "name" => "french",
          ],
          "elision" => [
            "type" => "elision",
            "articles" => [
              "l",
              "m",
              "t",
              "qu",
              "n",
              "s",
              "j",
              "d",
              "lorsqu",
            ],
          ],
          "autocomplete" => [
            "type" => "edge_ngram",
            "min_gram" => 2,
            "max_gram" => 5,
          ],
        ],
      ],
    ];

    $this->mappings[$name]['settings'] = $settings;
  }

  protected function buildMapping($name) {
    $params['index'] = $indexName = $this->getIndex($name);
    $newMapping = [];
    $mapping = $this->options['mapping'];
    foreach ($this->options['fields'][$name]['fields'] as $fieldName => $field) {
      if ($field instanceof BaseFieldDefinition || $field instanceof FieldConfigBase) {
        $type = $field->getType();
        $options = [
          'name' => $fieldName,
          'field' => $field,
          'mapping' => $mapping,
        ];
        $toField = XtcSearchLoaderFieldType::get($mapping[$type]['to'], $options);
        $newMapping[$fieldName] = $toField->formatTo();
      }
    }
    $this->mappings[$name]['index'] = $indexName;
    $this->mappings[$name]['mappings']['properties'] = $newMapping;
  }

  protected function getIndex($name) {
    return $this->options['fields'][$name]['index'];
  }

  protected function runProcess() {
    foreach ($this->mappings as $mapping => $settings) {
      $this->params = [
        'index' => $settings['index'],
        'body' => [
          'settings' => $settings['settings'],
          'mappings' => $settings['mappings'],
        ],
      ];
      $this->cleanParams();
      try {
        $this->content[$mapping] = $this->client->indices()
          ->create($this->params);
      } catch (\Exception $exception) {
        $this->content[$mapping] = Json::decode($exception->getMessage());
      }
      $this->getIndexMapping($settings['index'], $mapping);
    }
  }

  protected function cleanParams() {
    $params = [];
    foreach ($this->params as $name => $param) {
      if (in_array($name, self::AUTHORIZED)) {
        $params[$name] = $param;
      }
    }
    $this->params = $params;
  }

  static public function getLanguage($code) {
    return self::LANG[$code];
  }

}
