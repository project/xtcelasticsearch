<?php

namespace Drupal\xtcelasticsearch\Plugin\XtcHandler;


use Drupal\xtcelasticsearch\XtendedContent\API\IndexEntity;
use Drupal\xtcsearch\XtendedContent\Interfaces\IndexInterface;

/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "elasticsearch_index",
 *   label = @Translation("PHP Elasticsearch Index for XTC"),
 *   description = @Translation("PHP Elasticsearch Index for XTC description.")
 * )
 */
class ElasticSearchIndex extends ElasticSearchIndexBase implements IndexInterface {

  const AUTHORIZED = [
    'index',
    'type',
    'id',
    'consistency',
    'op_type',
    'parent',
    'refresh',
    'replication',
    'routing',
    'timeout',
    'timestamp',
    'ttl',
    'version',
    'version_type',
    'body',
  ];

  protected function adaptContent() {
    $this->content = IndexEntity::indexFields($this->content, $this->options);
  }

  protected function runProcess() {
    $this->params = array_merge($this->params, $this->options);
    $this->cleanParams();
    $this->params['body'] = $this->content;
    try {
      $this->content = $this->client->index($this->params);
    } catch (\Exception $exception) {
      $this->content = $exception->getMessage();
    }
  }

  protected function cleanParams() {
    foreach ($this->params as $name => $param) {
      if (in_array($name, self::AUTHORIZED)) {
        $params[$name] = $param;
      }
    }
    $this->params = $params;
  }

}
