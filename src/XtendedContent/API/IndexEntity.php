<?php


namespace Drupal\xtcelasticsearch\XtendedContent\API;


use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigBase;
use Drupal\xtcsearch\XtendedContent\API\XtcSearchLoaderFieldType;

class IndexEntity {

  /**
   * @param $fields
   * @param $mappingOptions
   *
   * @return array
   */
  static public function indexFields($fields, $mappingOptions){
    $mappedFields = [];
    $mapping = $mappingOptions['mapping'];
    foreach ($fields as $fieldName => $field) {
      $options = [
        'name' => $fieldName,
        'field' => $field,
        'mapping' => $mappingOptions,
      ];
      $definition = $field->getFieldDefinition();
      if ($definition instanceof BaseFieldDefinition || $definition instanceof FieldConfigBase) {
        $type = $definition->getType();
        $fromField = XtcSearchLoaderFieldType::get($mapping[$type]['from'], $options);
        $item = $fromField->setField()
          ->formatFrom();
        if (!empty($item)) {
          $mappedFields[$fieldName] = $item;
        }
      }
    }
    return $mappedFields;
  }

}
